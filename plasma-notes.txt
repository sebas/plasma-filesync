( * means todo or work in progress, o means done )
OwncloudFolder

o enum Status: Disabled, Waiting, Idle, Error
o enum Error: TimeSync, Connection, Disk, Other

o enable()
o disable()
o remove() (OwncloudFolder::removeSyncFolder)

OwncloudSettings:
o folders()
o add()
o remove()
o keep an eye on dbus service, update state accordingly
o start daemon machinery


TODO:

o start daemon automatically
o applet launches Settings or kcmshell4 when unconfigured
o fix up layout in add folder
o Login.qml - generic log in form w/ title, user, pass, url (line edit or list)
o kill owncloudsettings::statusmessage and ::errormessage
o swap names of owncloudsync and owncloudsyncdaemon
o Fix Grid layout warning in Login.qml (-> Item)
o move patches to github merge requests?
o fix last sync time
o "Sync Now!" button in folder delegates
o check if folder (local + remote) exist, create if necessary
o use errorMessage() in login widget
o pop up login widget automatically when NoConfiguration Error or Auth error occurs
o make sure add remote / local folder buttons appear correctly in delegate


1.0:
===
o make add custom folder work
o Fix workflow between screens
o Get patches on github upstreamed, ping Klaas after vacation
o Rename everything in the UI to Synchronization instead of owncloud
o preprend "Favorites" page with Pictures, Videos, Documents, Music pathes and checkboxen
o remove stuff from plasmoid, start kcmshell or active-settings from it
o new login should connect correctly
o login -> add folders when no folder are found
o no text in button in folder delegates
o remove only shown in settings
o links (in active-settings?) should point webbrowser

* pre-fill user name from settings, password?
* UI to handle ssl errors

later:
=====
* use mirall's new icons?
* add folder: check for both, local and remote folder if exists
* Settings page, with
  * slider-ish widget for polling interval
  * remote version information

* "Special" ownCloud folder, with metadata config therein, hidden in UI or
  shown with special cased delegate in folder list

* try switching to KIO::NAM, setter for owncloudInfo needed, testing
  needed (will csync go through NAM as well, does it matter to csync?) >> causes credential problems

* Investigate switching to KConfig & KWallet, ditching MirallConfigFile more or less,
  should go through QKeyChain

* Mechanism for adding Akonadi agents (from webaccounts?)
  * Calendar setup
  * Contacts setup
  * Monitoring akonadi sync

* VCS plugin

* Bookmarks
  * Rekonq
  * Firefox
  * Chrome




Sync:
o investigate polling with the goal to kill it for local folders
o retrieve list of remote folders in Mirall::ownCloudInfo
o Set and test KIO::NetworkAccessManager

Ideas
* loadable profiles, essentially folder configs with enabled / disabled, polling interval

o == done; * == todo


Testing:

= First use =

* delete ~/.local/share/data/Mirall
* restart owncloudsyncd
* Login page, leading to favorites after login
  - error message when credentials are wrong?
  - show favorites when logged in
    1 local folder does not exist, remote folder exists
    2 remote folder exists, local folder doesn't
    3 both local and remote don't exist
    4 both local and remote exist and are not empty
    5 folder already set up

Test setup:
- Documents: 5 Local files, empty remote
- Music: local empty, remote has 5 files
- InstantUpload: local doesn't exist, remote has photos


# Build with

cmakekde -DCSYNC_INCLUDE_PATH=ocsync/ -DPLASMACLIENT=ON

or export CSYNC_DIR, pointing to csync.h
