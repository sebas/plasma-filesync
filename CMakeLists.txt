
cmake_minimum_required(VERSION 2.6)

project(plasma-filesync)

set( CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/modules )

find_library(OWNCLOUDSYNC_LIBRARY owncloudsync)

find_package(Csync REQUIRED)
find_package(QtKeychain REQUIRED)
find_package(KDE4 REQUIRED)
find_package(KDeclarative REQUIRED)

include (KDE4Defaults)

add_subdirectory(src)
