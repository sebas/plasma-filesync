project( active_settings_owncloud )

set(owncloudsettings_SRCS
    owncloudsettingsplugin.cpp
)

kde4_add_plugin(active_settings_owncloud ${owncloudsettings_SRCS})

target_link_libraries(active_settings_owncloud
    ${KDE4_PLASMA_LIBS}
    ${QT_QTDECLARATIVE_LIBRARY}
    ${KDE4_KIO_LIBS}
    kocsync
)

install(TARGETS active_settings_owncloud DESTINATION ${PLUGIN_INSTALL_DIR})

