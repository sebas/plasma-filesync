set(kcm_owncloud_PART_SRCS
    kcmowncloud.cpp
    owncloudconfig.cpp
)

kde4_add_plugin(kcm_owncloud ${kcm_owncloud_PART_SRCS})

target_link_libraries(kcm_owncloud
    ${QT_QTDECLARATIVE_LIBRARY}
    ${KDE4_KCMUTILS_LIBS}
    ${KDE4_PLASMA_LIBS}
    ${KDECLARATIVE_LIBRARIES}
    ${KDE4_KIO_LIBS}
    kocsync
)

install(TARGETS kcm_owncloud DESTINATION ${PLUGIN_INSTALL_DIR})
install(FILES owncloudconfig.desktop DESTINATION ${SERVICES_INSTALL_DIR})
install(FILES settings-synchronization.desktop DESTINATION ${SERVICES_INSTALL_DIR})

