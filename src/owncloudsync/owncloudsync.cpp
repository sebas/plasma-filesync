/***************************************************************************
 *                                                                         *
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>                       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/


#include "owncloudsync.h"
#include "owncloudfolder.h"
#include "owncloudsettings.h"

#include <KIO/AccessManager>
#include <kdebug.h>
#include <QVariant>
#include <QTimer>

#include <owncloudsync/mirall/account.h>
#include <owncloudsync/mirall/syncresult.h>
#include <owncloudsync/mirall/folder.h>
#include <owncloudsync/mirall/mirallconfigfile.h>
#include <owncloudsync/mirall/progressdispatcher.h>
#include <owncloudsync/mirall/networkjobs.h>
#include <owncloudsync/creds/httpcredentials.h>

namespace Mirall {

class OwncloudSyncPrivate {
public:
    OwncloudSync *q;
    QString name;
    QString localPath;
    QString remotePath;
    QString display;

    QVariantMap folderList;
    QVariantMap owncloudInfo;
    QHash<QString, QVariantMap> folders;
    QHash<QString, QDateTime> syncTime;

    QTimer *delay;
    int ocStatus;
    int ocError;
};

OwncloudSync::OwncloudSync(QObject *parent)
    : QObject(parent)
{
    d = new OwncloudSyncPrivate;
    d->q = this;
    d->display = "";
    d->delay = 0;
    d->ocStatus = OwncloudSettings::Disconnected;
    d->ocError = OwncloudSettings::NoError;

    init();
}

using namespace Mirall;

void OwncloudSync::init()
{
    connect(FolderMan::instance(), SIGNAL(folderSyncStateChange(const QString&)),
             this,SLOT(slotSyncStateChange(const QString&)));

    connect(ProgressDispatcher::instance(),
            SIGNAL(progressInfo(const QString&, const Progress::Info&)),
            this,
            SLOT(progressInfo(const QString&, const Progress::Info&)));

    connect(ProgressDispatcher::instance(),
            SIGNAL(progressSyncProblem(const QString&, const Progress::SyncProblem&)),
            this,
            SLOT(progressSyncProblem(const QString&, const Progress::SyncProblem&)));

    connect(AccountManager::instance(), SIGNAL(accountChanged(Account*, Account*)),
            this, SLOT(slotAccountChanged(Account*, Account*)));


    Account *account = Account::restore();
    if (!account) {
        d->ocStatus = OwncloudSettings::Error;
        d->ocError = OwncloudSettings::NoConfigurationError;
        emit statusChanged(d->ocStatus);
        emit errorChanged(d->ocError);
    } else {
        qDebug() << "Account restored";
        account->credentials()->fetch(account);
    }
    //account->setSslErrorHandler(new SslDialogErrorHandler);
    AccountManager::instance()->setAccount(account);
}

OwncloudSync::~OwncloudSync()
{
    delete d;
}

FolderMan* OwncloudSync::folderMan()
{
    return FolderMan::instance();
}

Account* OwncloudSync::account()
{
    return AccountManager::instance()->account();
}

void OwncloudSync::slotAccountChanged(Account *newAccount, Account *oldAccount)
{
    qDebug() << "Account changed!" << newAccount << oldAccount;
    if (oldAccount) {
        disconnect(oldAccount, SIGNAL(stateChanged(int)), this, SLOT(slotAccountStateChanged(int)));
    }
    if (newAccount) {
        connect(newAccount, SIGNAL(stateChanged(int)), this, SLOT(slotAccountStateChanged(int)));
        connect(newAccount->credentials(), SIGNAL(fetched()), this, SLOT(slotCheckAuthentication()));
    }
}

void OwncloudSync::slotSyncStateChange(const QString &s)
{
    Folder *f = FolderMan::instance()->folder(s);
    if (f && (f->syncResult().status() == SyncResult::Success)) {
        d->syncTime[s] = QDateTime::currentDateTime();
//         qDebug() << "OC updated syncTime for " << s << d->syncTime[s];
    }
    updateFolder(FolderMan::instance()->folder(s));
}

void OwncloudSync::progressInfo(const QString& folder, const Progress::Info& progress)
{
/* // From progressdispatcher.h
    enum Kind {
        Invalid,
        StartSync,
        Download,
        Upload,
        Context,
        Inactive,
        StartDownload,
        StartUpload,
        EndDownload,
        EndUpload,
        EndSync,
        StartDelete,
        EndDelete,
        StartRename,
        EndRename,
        SoftError,
        NormalError,
        FatalError
    };

    struct Info {
        Kind    kind;
        QString folder;
        QString current_file;
        QString rename_target;

        qint64  file_size;
        qint64  current_file_bytes;

        qint64  overall_file_count;
        qint64  current_file_no;
        qint64  overall_transmission_size;
        qint64  overall_current_bytes;

        QDateTime timestamp;
    }
*/
    QVariantMap vm;
    vm["folder"] = progress.folder;
    vm["file_size"] = progress.file_size;
    vm["current_file"] = progress.current_file;
    vm["current_file_no"] = progress.current_file_no;
    vm["current_file_bytes"] = progress.current_file_bytes;

    vm["overall_file_count"] = progress.overall_file_count;
    vm["overall_transmission_size"] = progress.overall_transmission_size;
    vm["overall_current_bytes"] = progress.overall_current_bytes;
    vm["rename_target"] = progress.rename_target;
    vm["timestamp"] = progress.timestamp;
    vm["kind"] = Progress::asResultString(progress);

    qDebug() << "Progress! : " << vm;
    emit progressChanged(vm);
}

void OwncloudSync::progressSyncProblem(const QString& folder, const Progress::SyncProblem& problem)
{
    qDebug() << "Not doing anything here.";
}

QString OwncloudSync::display()
{
    //qDebug() << "OC display() " << d->display;
    return d->display;
}

QVariantMap OwncloudSync::folder(QString name)
{
    return d->folders[name];
}

void OwncloudSync::enableFolder(const QString &name, bool enabled)
{
    qDebug() << " OC enableFolder: " << name << enabled;
    Folder *f = FolderMan::instance()->folder(name);
    if (f) {
        f->setSyncEnabled(enabled);
        updateFolder(f);
    } else {
        qWarning() << "Folder \"" << name << "\" does not exist.";
    }
}

void OwncloudSync::syncFolder(const QString& name)
{
    qDebug() << " OC syncFolder: " << name;
    Folder *f = FolderMan::instance()->folder(name);
    if (f) {
        f->startSync(QStringList());

    } else {
        qWarning() << "Folder \"" << name << "\" does not exist.";
    }
}

void OwncloudSync::cancelSync(const QString& name)
{
    qWarning() << "OC Terminating a sync operation is not implemented yet.";
    //qDebug() << " OC syncFolder: " << name;
    Folder *f = FolderMan::instance()->folder(name);
    if (f) {
        qWarning() << "Terminating a sync operation is not implemented yet.";
    } else {
        qWarning() << "Folder \"" << name << "\" does not exist.";
    }
}

QVariantMap OwncloudSync::folderList()
{
    return d->folderList;
}

void OwncloudSync::refresh()
{
    qDebug() << "POC Syncdaemon refresh()";
    if (!account()) {
        d->ocStatus = OwncloudSettings::Error;
        d->ocError = OwncloudSettings::NoConfigurationError;
        qDebug() << "No config";
    } else {
        loadFolders();
        qDebug() << "POC We're good" << (d->ocStatus != OwncloudSettings::Disconnected) << d->ocStatus;
    }
    qDebug() << "emitting changed signals, connected? " << (d->ocStatus == OwncloudSettings::Connected);
    emit statusChanged(d->ocStatus);
    emit errorChanged(d->ocError);
    emit owncloudChanged(d->owncloudInfo);

}

void OwncloudSync::loadFolders()
{
    qDebug() << "Loadfolders..." << d->folderList;
    if (d->ocStatus == OwncloudSettings::Connected) {
        QStringList fs;
        foreach (Folder* f, FolderMan::instance()->map()) {
            connect(f, SIGNAL(syncFinished(const SyncResult&)), SLOT(folderSyncFinished(const SyncResult&)));
            updateFolder(f);
        }
    } else {
        qDebug() << "Not connected";
        d->folderList.clear();
        d->folders.clear();
        emit folderListChanged(d->folderList);
    }
}

void OwncloudSync::folderSyncFinished(const SyncResult &r)
{
    if (r.status() == SyncResult::Success) {
        Folder *f = static_cast<Folder*>(sender());
        if (f) {
            d->syncTime[f->alias()] = QDateTime::currentDateTime();
        } else {
            //qDebug() << " OC no folder found";
        }
    }
}

QString errorMsg(int r) {
    QString s;
    if (r == SyncResult::Success) s = "SyncResult::Success -> OwncloudFolder::Idle";
    if (r == SyncResult::Error) s = "SyncResult::Error -> OwncloudFolder::Error";
    if (r == SyncResult::NotYetStarted) s = "SyncResult::NotYetStarted -> OwncloudFolder::Waiting";
    if (r == SyncResult::SyncRunning) s = "SyncResult::SyncRunning -> OwncloudFolder::Running";
    if (r == SyncResult::SetupError) s = "SyncResult::SetupError -> OwncloudFolder::Error";
    if (r == SyncResult::Undefined) s = "SyncResult::Undefined -> OwncloudFolder::Error";
    return s;
}

void OwncloudSync::updateFolder(const Folder* folder)
{
    QVariantMap m;
    m["name"] = folder->alias();
    m["localPath"] = folder->path();
    m["remotePath"] = folder->remotePath();
    m["syncTime"] = d->syncTime[folder->alias()].toMSecsSinceEpoch();
//     qDebug() << "OC updateFolder:: path, secondPath: " << folder->path() << ", " << d->syncTime[folder->alias()];

    int s = 999;
    int r = folder->syncResult().status();
    //qDebug() << " c" << c << " r" << r;
    if (folder->syncEnabled()) {
        if (r == SyncResult::Success) s = OwncloudFolder::Idle;
        if (r == SyncResult::Error) s = OwncloudFolder::Error;
        if (r == SyncResult::NotYetStarted) s = OwncloudFolder::Waiting;
        if (r == SyncResult::SyncRunning) s = OwncloudFolder::Running;
        if (r == SyncResult::SetupError) s = OwncloudFolder::Error;
        if (r == SyncResult::Undefined) s = OwncloudFolder::Error;
    } else {
        s = OwncloudFolder::Disabled;
    }
    if (r == 999) {
        s = OwncloudFolder::Error;
    }
    m["status"] = s;

    if (s == OwncloudFolder::Error) {
        m["errorMessage"] = folder->syncResult().errorString();
    } else {
        m["errorMessage"] = QString();
    }

    d->folders[folder->alias()] = m;
    //qDebug() << " OC FOLdERs: " << folder->alias() << folder->syncResult().statusString();
    emit folderChanged(m);
}

void OwncloudSync::addSyncFolder(const QString& localFolder, const QString& remoteFolder, const QString& alias)
{
    FolderMan::instance()->addFolderDefinition(alias, localFolder, remoteFolder);

    qDebug() << "POC OCD OwncloudSyncDaemon::addSyncFolder: " << localFolder << remoteFolder << alias;

    FolderMan::instance()->setupFolders();
    refresh();
}

void OwncloudSync::removeSyncFolder(const QString& alias)
{
    FolderMan::instance()->slotRemoveFolder(alias);

    qDebug() << "OCD OwncloudSyncDaemon::removeSyncFolder: " << alias;

    if (!d->delay) {
        d->delay = new QTimer(this);
        d->delay->setSingleShot(true);
        d->delay->setInterval(2000);
        connect(d->delay, SIGNAL(timeout()), SLOT(delayedReadConfig()));
    }
    d->delay->start();
    d->folders.remove(alias);
    FolderMan::instance()->setupFolders();
    refresh();
}

void OwncloudSync::delayedReadConfig()
{
    FolderMan::instance()->setupFolders();
    refresh();
}

void OwncloudSync::checkRemoteFolder(const QString& f)
{
    if (d->ocStatus == OwncloudSettings::Connected) {
        const QString remotePath = QString("/").append(f);
        qDebug() << "checking folder ... ?" << remotePath;
        LsColJob *job = new LsColJob(AccountManager::instance()->account(), remotePath, this);
        connect(job, SIGNAL(directoryListing(QStringList)),
                SLOT(slotCheckRemoteFolderFinished()));
        connect(job, SIGNAL(networkError(QNetworkReply*)),
                SLOT(slotCheckRemoteFolderFinished()));
        job->start();
    }
}

void OwncloudSync::slotDirectoryListingUpdated(const QStringList &directories )
{
    qDebug() << "POC found direvotories" << directories;
}


void OwncloudSync::slotCheckRemoteFolderFinished()
{
    qDebug() << "REmote folder exists ... ?";
    AbstractNetworkJob* j = static_cast<AbstractNetworkJob*>(sender());
    if (j) {
        bool exists = j->reply()->error() == QNetworkReply::NoError;
        QString p = j->reply()->url().toString().split("remote.php/webdav/")[1];
        qDebug() << " Folder exists ? " << p << exists;
        emit remoteFolderExists(p, exists);
    }
}

void OwncloudSync::slotDirCheckReply(const QString &url, QNetworkReply *reply)
{
    //qDebug() << " OC slotDirCheckReply" << url <<  (reply->error() == QNetworkReply::NoError) << (int)(reply->error());
    emit remoteFolderExists(url, reply->error() == QNetworkReply::NoError);
}

void OwncloudSync::createRemoteFolder(const QString &f)
{
    if(f.isEmpty()) return;

    qDebug() << "OC creating folder on ownCloud: " << f;

    MkColJob *job = new MkColJob(account(), f, this);
    connect(job, SIGNAL(finished(QNetworkReply::NetworkError)),
                 SLOT(slotCreateRemoteFolderFinished(QNetworkReply::NetworkError)));
    job->start();
}

void OwncloudSync::slotCreateRemoteFolderFinished(QNetworkReply::NetworkError err)
{
    AbstractNetworkJob *job = static_cast<AbstractNetworkJob*>(sender());
    //QNetworkReply* reply = static_cast<QNetworkReply*>(sender());
    bool exists = err == QNetworkReply::NoError;
    //QString p = reply->url().toString().split("remote.php/webdav/")[1];
    const QString p = job->path();
    qDebug() << " === OC slot -- CREATE -- RemoteFolderFinished() : " << p << exists;
    emit remoteFolderExists(p, exists);
}

void OwncloudSync::slotAccountStateChanged(int state)
{
    if (state == Account::Connected) {
        d->owncloudInfo["url"] = account()->url();
    } else if (state == Account::Disconnected) {
        d->owncloudInfo["url"] = QString();
    }
}

void OwncloudSync::slotInstanceFound(const QUrl&url, const QVariantMap &info)
{
    qDebug() << "OCD : ownCloud found: " << url << " with info " << info;

    d->owncloudInfo = info;
    d->owncloudInfo["url"] = url.toString();
    emit owncloudChanged(d->owncloudInfo);
}

void OwncloudSync::slotNoOwnCloudFound(QNetworkReply* reply)
{
    Q_UNUSED(reply)
    d->ocStatus = OwncloudSettings::Error;
    d->ocError = OwncloudSettings::NoConfigurationError;
    qDebug() << "POC : slotNoOwnCloudFound";
    emit statusChanged(d->ocStatus);
    emit errorChanged(d->ocError);
}

void OwncloudSync::slotCheckAuthentication()
{
    if (!AccountManager::instance()->account()) {
        return;
    }
    qDebug() << "OwncloudSync::slotCheckAuthentication()";
    LsColJob *job = new LsColJob(AccountManager::instance()->account(), "/", this);
    connect(job, SIGNAL(directoryListing(QStringList)),
            SLOT(slotAuthCheck()));
    connect(job, SIGNAL(networkError(QNetworkReply*)),
            SLOT(slotAuthCheck()));

    connect(job, SIGNAL(directoryListing(QStringList)),
            SLOT(showDirList(QStringList)));

    job->start();
}

void OwncloudSync::showDirList(QStringList dirs)
{
    qDebug() << "DirList : " << dirs;
}

void OwncloudSync::slotAuthCheck()
{
    qDebug() << "+++ OC auth check";
    AbstractNetworkJob *j = qobject_cast<AbstractNetworkJob*>(sender());
    if (!j) {
        qWarning() << "slotAuthCheck not called from an AbstractNetworkJob";
        return;
    }
    QNetworkReply *reply = j->reply();
    if( reply->error() == QNetworkReply::AuthenticationRequiredError ) { // returned if the user is wrong.
        if (d->ocStatus != OwncloudSettings::Error ||
                            d->ocError != OwncloudSettings::AuthenticationError) {
            qDebug() << "OC ******** Password is wrong!";
            d->ocStatus = OwncloudSettings::Error;
            d->ocError = OwncloudSettings::AuthenticationError;
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
        }
    } else if( reply->error() == QNetworkReply::OperationCanceledError ) {
        // the username was wrong and ownCloudInfo was closing the request after a couple of auth tries.
        qDebug() << "OC ******** Username or password is wrong!";
        if (d->ocStatus != OwncloudSettings::Error ||
                            d->ocError != OwncloudSettings::AuthenticationError) {
            d->ocStatus = OwncloudSettings::Error;
            d->ocError = OwncloudSettings::AuthenticationError;
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);
        }
    } else if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "OC ######## Credentials are ok!";
        qDebug() << "POC CHECK statusChanged connected?" << (d->ocStatus == OwncloudSettings::Connected);
        if (d->ocStatus != OwncloudSettings::Connected) {
            qDebug() << "OC ****** changing to Connected/NoError! :-)";
            d->ocStatus = OwncloudSettings::Connected;
            d->ocError = OwncloudSettings::NoError;
            qDebug() << "________ POC emit statusChanged " << (d->ocStatus == OwncloudSettings::Connected);
            emit statusChanged(d->ocStatus);
            emit errorChanged(d->ocError);

//             void instanceFound(const QUrl&url, const QVariantMap &info);
//             void timeout(const QUrl&url);
            qDebug() << "________ POC CheckServerJob ";

            CheckServerJob *cj = new CheckServerJob(AccountManager::instance()->account());
            connect(cj, SIGNAL(instanceFound(const QUrl&, const QVariantMap&)),
                    this, SLOT(slotInstanceFound(const QUrl&, const QVariantMap&)));
            cj->start();
            qDebug() << "setup folders";
            FolderMan::instance()->setupFolders();
            qDebug() << "load folders";
            loadFolders();
        }
    } else {
        qDebug() << "Unspecified network error!";
        d->ocStatus = OwncloudSettings::Error;
        d->ocError = OwncloudSettings::NetworkError;
        emit statusChanged(d->ocStatus);
        emit errorChanged(d->ocError);
    }
}

void OwncloudSync::setupOwncloud(const QString &server, const QString &user, const QString &password)
{
    MirallConfigFile cfgFile;
    cfgFile.setRemotePollInterval(600000); // ten minutes for now

    bool https = server.startsWith("https");

    QString _srv = server;
    if (!server.endsWith('/')) {
        _srv.append('/');
    }

    Account* account = new Account;
    account->setUrl(QUrl(_srv));
    HttpCredentials *creds = new HttpCredentials(user, password);
    account->setCredentials(creds);
    account->save();
    AccountManager::instance()->setAccount(account);

    if( FolderMan::instance() ) {
        FolderMan::instance()->removeAllFolderDefinitions();
    }

    slotCheckAuthentication();
}

void OwncloudSync::slotGuiLog(const QString& err, const QString& msg)
{
    qDebug() << "guilog() POC" << err << msg;
}

}// namespace

#include "owncloudsync.moc"

