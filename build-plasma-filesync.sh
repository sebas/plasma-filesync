#!/bin/sh

echo $KDEDIR
cd $KDEDIR/../build/plasma-filesync
#cd $KDEDIR/../build/
#$mkdir mirall
#cd mirall
pwd
cmake -DCMAKE_INSTALL_PREFIX=$KDEDIR \
      -DCMAKE_BUILD_TYPE="Debug"  \
      -DCSYNC_BUILD_PATH=$KDEDIR/../build/ocsync \
      -DCSYNC_INCLUDE_PATH=$KDEDIR/include/ocsync \
      $KDEDIR/../src/plasma-filesync
